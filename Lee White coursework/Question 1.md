In simple terms, a stakeholder is a party that supports a company and can either affect or be affected by the business. Within this software development project, there are many stakeholders that will have an influence on the product that is delivered. There are stakeholders that we need to discuss the requirements of the project with, to ensure that an accurate, achievable and realistic expectation can be met.

The first key stakeholder that the team would need to consult with is the customer. It is vital that we talk to the customer as they would be the key stakeholder that provides us with the list of requirements and exigencies that we need to meet in order for the product to be desirable. This phase will enable the team to see if it is a feasible project with reasonable tasks to meet, and will allow the planning stage to begin as organisation for the dedicated tasks can take place. It will also give an idea of the funding that will be required and the resources needed in order to meet the requirements given by the customer.

Along with the customer, another stakeholder in relation to this associate are the students that we are designing the game for. It would be desirable to talk to the students as they are the target demographic that the project is being made for, thus gaining feedback from them would be vital as it would enable the development team to make any necessary adjustments to satisfy the users. This would also give us a chance to see if there are any extra versions of the game that may need to be developed or modified in order to suit all users (eg, potential students that may have disabilities) 

Another stakeholder that would need to be discussed with are associates of the government. This would be important as it would be beneficial to get an understanding of how much funding can be received for the project to take place. This would enable the team to allocate resources and equipment to other stakeholders such as programmers and designers who would be developing and designing the game, allowing them to get a clear understanding of the tools and resources they would have to work with.  





Lee White 
30021161
