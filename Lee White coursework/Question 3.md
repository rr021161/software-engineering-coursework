It is important to note that there are many issues and constraints that can arise and hinder the progress within a software development project, especially one catered towards creating a video game. Not only do common project constraints such as; time, scope and funding have major repercussions but other constraints such as equipment, staff, technical capabilities and legal issues (such as DMCA and copyright laws) need to be addressed. 


The first and most important constraint to consider is time. It is crucial that we plan and organise our time in a professional and efficient manner in order to deliver the project on time to our customer. This can be achieved by creating a decisive project scope which will enable us to document the project goals, deliverables, tasks, costs and deadlines. This also allows us to implement a work structure for other team members who are working on developing the video game for the students. If we fail to avoid this constraint, then the whole project could be at risk, as it will be excruciatingly hard to backtrack (depending on the software development cycle used) and fulfill the customers orders within a shortened timespan.

Funding is also another constraint that could potentially cause some issues around project development. It is vital that the team has enough funding in order to purchase the resources and equipment needed to develop the project. Without proper equipment, the team will be unable to develop the game required for the consumer within an efficient manner and could have to cut costs on game design (such as assets, sprites etc). Technical capabilities will also be limited if premium equipment is unable to be retained.

Another big constraint that affects this individual project around developing a game is the DMCA legal requirement. As we are developing a game, certain assets may need to be used (such as sprites, video game characters, designs, images etc) and it is critical that this project adheres to the copyright act, as otherwise the game we are creating would withhold illegal assets which could lead the team in arduous legal trouble, which would not only affect funding but would also ruin the reputation of the development team and could draw the whole project to an end.




Lee White 
30021161
