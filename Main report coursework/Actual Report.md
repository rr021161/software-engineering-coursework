Introduction

Before creating an education game the programmar/developer needs to make sure and clarify all the requirements and points to make the best video game that will help the students improve on their reading this can through puzzle, adventure or even a memory based game where they would have to read and recall informations to advancnce to the next level. Without making points with the stakeholders the developer can run into problems when creating the game as they would not know what kind of game they would want which can make students not improve. It is very important to take points from the stakeholders before the developing the game to check if they meet requirements, have sufficiant funds and even save money and build a good understanding between each stakeholders.

Stakeholders

When it comes to different stakeholders that will support this program it varies from students, lecturers, government and even the programmers that help creating the game. It affects students as the game that will be created is to encourage and help them to read more while playing video games. It also affects lecturers as there is a high chance that this so called educational video game will distract students from doing uni work which can put them behind in certain courses which will affect everyone. Governments are stakeholders because they will give support to university to help create a educational game to support students and their future to read more. Another reason why students and lectures are stakeholders is that when creating the game they can ask for requirements on what area to improve on such as reading and finding information or just in general to read and understand the task by creating the game the lectures can monitor the progress of each students on how they are improving or if its i affecting their progress this can then be reported back to the government which then can be used as reference to come up with a bigger plan to help students in many different ways. Programmers are stakeholders because they will be the ones to create the game and help it improve with feedbacks that will be given to them by the surveys held by the university that is using this educational game scheme.


Key points 

- Incorporate material from different modules into the game effectively and efficiently
- Easy to use for students and teachers
- Allows students to learn lecture material
- Different game modes e.g puzzle, question game, first person game
- Precise rules and instructions
- Have different difficulty levels
- Have answers and feedback at the end of the game
- Goals and objectives students need to meet at the end of the game
- Have bright colours to visually stimulate and attract students
- Could have multiplayer competition challenges with other students
- Replayability if it is a single player game
- Worthwhile long term goals as well as short term goals if applicable
- Easy and intuitive controls
- Has a high-score table


A key game mechanic that could be implemented within the game would be achievements, as it allows the player to celebrate what they have achieved or allows them to show what they are capable of, it can also increase the difficulty of the game depending on how hard an achievement is achievable. Another game mechanic that can be implemented could be a countdown mechanic if the game is a puzzle type and not a first person game, the countdown feature would be able to add pressure to the player and after each play through they can get better at answering questions quicker.


Constraints

It is important to note that there are many issues and constraints that can arise and hinder the progress within a software development project, especially one catered towards creating a video game. Not only do common project constraints such as; time, scope and funding have major repercussions but other constraints such as equipment, staff, technical capabilities and legal issues (such as DMCA and copyright laws) need to be addressed.
The first and most important constraint to consider is time. It is crucial that we plan and organise our time in a professional and efficient manner in order to deliver the project on time to our customer. This can be achieved by creating a decisive project scope which will enable us to document the project goals, deliverables, tasks, costs and deadlines. This also allows us to implement a work structure for other team members who are working on developing the video game for the students. If we fail to avoid this constraint, then the whole project could be at risk, as it will be excruciatingly hard to backtrack (depending on the software development cycle used) and fulfill the customers orders within a shortened timespan.
Funding is also another constraint that could potentially cause some issues around project development. It is vital that the team has enough funding in order to purchase the resources and equipment needed to develop the project. Without proper equipment, the team will be unable to develop the game required for the consumer within an efficient manner and could have to cut costs on game design (such as assets, sprites etc). Technical capabilities will also be limited if premium equipment is unable to be retained.
Another big constraint that affects this individual project around developing a game is the DMCA legal requirement. As we are developing a game, certain assets may need to be used (such as sprites, video game characters, designs, images etc) and it is critical that this project adheres to the copyright act, as otherwise the game we are creating would withhold illegal assets which could lead the team in arduous legal trouble, which would not only affect funding but would also ruin the reputation of the development team and could draw the whole project to an end.


Clarification of Key points

Within this project we found many points which seemed to hinder the performance and goals of this project. Many constraints were minor and hence weren’t prioritized however we found some points which could have been a deal breaker to the video game.
One requirement which needed to be prioritised was the incorporation of material from different modules. This is a constraint and affects the length of the project greatly as we must ensure as developers that the information, we are providing to the university students are correct and accurate. Adding inaccurate or wrong information can be detrimental, this is because the university students will use the video game to learn material for exams. Implementing incorrect information can lead to students learning the wrong information which can impact their exams. One way we can address this is by ensuring a team within the project can focus on quality control and ensuring everything implemented is correct and approved by the customer.
Another requirement that we found which could be useful to the project is the application of bright colours to visually stimulate the students and keep them attentive. As we have been told by the customer, we are trying to encourage the university students to do their set readings. Creating a dull and boring interface can be horrible to learn from and can have negative outcomes. Looking at the psychology of colours, we can find that the colour green has a soothing effect on the eyes and alleviated stress. Incorporating these will help us reach the customers desires and needs and will allow us to create a successful product.
Another requirement which we deemed to be incredibly important is the implementation of live answers and feedback. We found this requirement to be important as it actively encourages the students to go back and learn from their mistakes. Having feedback will give them constructive criticism which will allow them to improve their confidence and self-awareness and the customer should see an increase in assessment performance. Due to lack of information provided by the customer we hope that implementing feedback and possibly a high score table will encourage a competitive nature among the students and will create an urge to improve either their previous score or outperform their peers.
The last potential deal-breaker would be if the game requires a high level of graphical hardware to function properly, as most school settings do not have top of the line computers for these demanding games to be run. Due to this, the game has to be not demanding enough for school computers to run smoothly.

Conclusion

In conclusion, after analysing the key requirements we believe that we should develop a memory based game with additional puzzles implemented within the game as this will enable students to read information and help them solving the puzzles. When comparing our game against the requirements, we believe our game will be suitable and easy to run on the students hardware. As programmers, this project will be easy to manage as it will be a light program to develop meaning it will be easy to add or modify and features based on the feedback from the students in order to improve their reading abilities. We also plan on making our game competitive in order to encourage students to try harder and compete against their peers, which is not only fun for them, but will also conciously improve their learning. As our game will be light and cost effective, it also means that we should not run into any issues with funding or resources, enabling us to develop the project on time and meet our deadline with our consumer. 

